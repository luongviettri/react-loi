import './App.css';
import BaiTapComponent from './components/BaiTap/BaiTapComponent';
import BaiTapState from './components/BaiTapState/BaiTapState';
import { DemoVongLap } from './components/BaiTapVongLap/DemoVongLap';
import Event from './components/DataBinding/Event';
import SinhVien from './components/DataBinding/SinhVien';
import DemoProps from './components/DemoProps';
import BaiTapGioHang from './components/Props/BaiTapGioHang/BaiTapGioHang';
import BaiTapTruyenFunction from './components/Props/BaiTapTruyenFunction/BaiTapTruyenFunction';
import SanPham_RCC from './components/Props/SanPham_RCC';
import SanPham_RFC from './components/Props/SanPham_RFC';
import DemoIf from './components/ReRender/DemoIf';


function App() {
  return (
    <div className="App">
      {/* <BaiTapComponent /> */}
      {/* <SinhVien /> */}
      {/* <Event /> */}
      {/* <DemoIf /> */}
      {/* <BaiTapState /> */}
      {/* <DemoVongLap /> */}
      {/* <DemoProps title="duLieuDuocTruyenDi" />
      <SanPham_RFC example="OK" title="Something" />
      <SanPham_RCC something=" some really think" /> */}
      {/* <BaiTapTruyenFunction /> */}
      <BaiTapGioHang />
    </div>
  );
}

export default App;
