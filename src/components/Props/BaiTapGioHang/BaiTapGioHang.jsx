import React, { Component } from 'react'
import Model from './Model';
import SanPham from './SanPham';
import ThongTinChiTiet from './ThongTinChiTiet';

import data from '../../../data/dataPhone.json'


export default class BaiTapGioHang extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            sanPhamChiTiet: { "maSP": 1, "tenSP": "VinSmart Live", "manHinh": "AMOLED, 6.2, Full HD+", "heDieuHanh": "Android 9.0 (Pie)", "cameraTruoc": "20 MP", "cameraSau": "Chính 48 MP & Phụ 8 MP, 5 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 5700000, "hinhAnh": "./img/vsphone.jpg" },
            sanPhamGioHang: {}
        })
    }

    mangSanPham = [
        { "maSP": 1, "tenSP": "VinSmart Live", "manHinh": "AMOLED, 6.2, Full HD+", "heDieuHanh": "Android 9.0 (Pie)", "cameraTruoc": "20 MP", "cameraSau": "Chính 48 MP & Phụ 8 MP, 5 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 5700000, "hinhAnh": "./img/vsphone.jpg" },
        { "maSP": 2, "tenSP": "Meizu 16Xs", "manHinh": "AMOLED, FHD+ 2232 x 1080 pixels", "heDieuHanh": "Android 9.0 (Pie); Flyme", "cameraTruoc": "20 MP", "cameraSau": "Chính 48 MP & Phụ 8 MP, 5 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 7600000, "hinhAnh": "./img/meizuphone.jpg" },
        { "maSP": 3, "tenSP": "Iphone XS Max", "manHinh": "OLED, 6.5, 1242 x 2688 Pixels", "heDieuHanh": "iOS 12", "cameraSau": "Chính 12 MP & Phụ 12 MP", "cameraTruoc": "7 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 27000000, "hinhAnh": "./img/applephone.jpg" }
    ]


    // ! Phương thức cần thiết:
    xemChiTiet = (maSP) => {
        let SPcanTim = this.mangSanPham.find((sp) => {
            return sp.maSP === maSP;
        })
        this.setState({
            sanPhamChiTiet: SPcanTim
        })
    }

    renderSanPham = () => {
        let sanPhamCanRender = this.mangSanPham.map((sp) => {
            return (
                <SanPham
                    sp={sp}
                    xemChiTiet={this.xemChiTiet}
                    key={sp.maSP}
                    layThongTinSanPham={this.layThongTinSanPham}
                />
            )
        })
        return sanPhamCanRender;
    }

    timSanPham = (maSP) => {
        return (
            this.mangSanPham.find((sp) => {
                return sp.maSP === maSP
            })
        )
    }

    layThongTinSanPham = (maSP) => {
        // console.log('maSP: ', maSP);
        let sanPhamCanTim = this.timSanPham(maSP);
        this.setState({
            sanPhamGioHang: sanPhamCanTim
        })

    }



    render() {
        // console.log('dataPhone: ', dataPhone);

        // ! Xác định cái gì thay đổi ( state):
        // ! Xác định component nào là cha để định nghĩa callback function
        // ! thực hiện
        return (
            <>

                <div className="container">
                    <h1 className='text-center my-4 text-success' >Bài tập giỏ hàng</h1>
                    {/*  chỗ này để render các sản phẩm */}
                    <div className='row '>
                        {this.renderSanPham()}
                    </div>
                    {/* Chỗ này làm thông tin chi tiết */}
                    <ThongTinChiTiet
                        chiTiet={this.state.sanPhamChiTiet}
                    />
                    {/* Chỗ này làm Model */}
                    <div>
                        <Model
                            chiTiet={this.state.sanPhamGioHang}
                        />

                    </div>
                </div>
            </>
        )
    }
}
