import React, { Component } from 'react'

export default class Model extends Component {

    constructor(props) {
        super(props);
        this.state = ({
            soLuong: 1
        })
    }
    // ! hàm tăng số lượng
    tangSoluong = () => {
        this.setState({
            soLuong: this.state.soLuong + 1
        })
    }
    // ! hàm giảm số lượng
    giamSoluong = () => {
        this.setState({
            soLuong: this.state.soLuong - 1
        })
    }
    render() {
        let { chiTiet } = this.props
        return (
            <div>
                <div className="modal fade" id="modelId" tabIndex={-1} role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <div className="modal-title ">
                                    <div className="row text-center">
                                        <p className='col-6'>Mã sản phẩm</p>
                                        <p className='col-6'> {chiTiet.maSP} </p>
                                    </div>
                                    <div className="row text-center">
                                        <p className='col-6'>Hình ảnh</p>
                                        <img src=
                                            {chiTiet.hinhAnh}
                                            className="w-100 col-6"

                                        />
                                    </div>
                                </div>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row text-center">
                                    <p className='col-6' >Tên sản phẩm</p>
                                    <p className='col-6' >{chiTiet.tenSP}</p>
                                </div>
                                <div className="row text-center">
                                    <p className='col-6'>
                                        Số lượng
                                    </p>
                                    <div className='col-6'>
                                        <a href="" className='btn btn-primary'
                                            onClick={(e) => {
                                                e.preventDefault();
                                                this.giamSoluong()
                                            }}
                                        >-</a>
                                        <span>
                                            {this.state.soLuong}
                                        </span>
                                        <a
                                            href=""
                                            className='btn btn-primary'
                                            onClick={(e) => {
                                                e.preventDefault();
                                                this.tangSoluong()
                                            }}
                                        >+</a>
                                    </div>
                                </div>
                                <div className="row text-center">
                                    <p className='col-6'>Đơn giá</p>
                                    <p className='col-6'>{chiTiet.giaBan}</p>
                                </div>

                                <div className="row text-center">
                                    <p className='col-6'>Thành tiền</p>
                                    <p className='col-6'>{chiTiet.giaBan * this.state.soLuong}</p>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
