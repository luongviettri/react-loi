import React, { Component } from 'react'

export default class SanPham extends Component {
    render() {
        let { sp, xemChiTiet, layThongTinSanPham } = this.props;
        return (
            <div className="col-4 " key={sp.maSP} >
                <div className="card">
                    <img className="card-img-top rounded" style={{ height: "300px" }} src={sp.hinhAnh} />
                    <div className="card-body">
                        <h4 className="card-title">{sp.tenSP}</h4>
                        <a
                            onClick={(event) => {
                                event.preventDefault();
                                xemChiTiet(sp.maSP);
                            }}
                            href="#"
                            className='btn btn-success mr-3' >
                            Xem chi tiết
                        </a>
                        <a
                            onClick={() => {
                                layThongTinSanPham(sp.maSP);
                            }}
                            href="#"
                            className='btn btn-danger'
                            data-toggle="modal"
                            data-target="#modelId" >
                            Thêm giỏ hàng</a>
                    </div>
                </div>
            </div>
        )
    }
}
