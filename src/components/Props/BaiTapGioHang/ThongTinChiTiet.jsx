import React, { Component } from 'react'

export default class ThongTinChiTiet extends Component {
    render() {
        let { chiTiet } = this.props;
        return (
            <div className='row' >
                <div className='col-6 text-center'>
                    <h3>{chiTiet.tenSP}</h3>
                    <div>
                        <img src={chiTiet.hinhAnh} alt="" />
                    </div>
                </div>
                <div className='col-6'>
                    <h2>Thông số kĩ thuật</h2>
                    <div className="row my-5">
                        <div className="col-6">
                            <p>
                                Màn hình
                            </p>
                            <p>
                                Hệ điều hành
                            </p>
                            <p>
                                Camera trước
                            </p>
                            <p>
                                Camera sau
                            </p>
                            <p>
                                RAM
                            </p>
                            <p>
                                ROM
                            </p>
                        </div>
                        <div className="col-6 ">
                            <p>
                                {chiTiet.manHinh}
                            </p>
                            <p>
                                {chiTiet.heDieuHanh}
                            </p>
                            <p>
                                {chiTiet.cameraTruoc}

                            </p>
                            <p>
                                {chiTiet.cameraSau}
                            </p>
                            <p>
                                {chiTiet.ram}
                            </p>
                            <p>
                                {chiTiet.rom}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
