import React, { Component } from 'react'

export default class DemoIf extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: false,
            userName: ''
        }
    }
    // ! Thuộc tính


    // ! Phương thức
    login = () => {
        this.setState({
            isLogin: true,
            userName: 'Luong Viet Tri'
        }, () => {
            console.log('this.state: ', this.state);
        })

    }
    logout = () => {
        this.setState({
            isLogin: false,
            userName: ''
        }, () => {
            console.log('this.state: ', this.state);
        })

    }


    // ! Phương thức render
    render() {
        return (
            <div>
                {this.state.isLogin ?
                    <>
                        <h1>Hello Lương Việt Trí</h1>
                        <button onClick={this.logout} >Đăng xuất</button>
                    </>
                    :
                    <>
                        <button onClick={this.login} >Đăng nhập</button>
                    </>
                }
            </div>
        )
    }
}
