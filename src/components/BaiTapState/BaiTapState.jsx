import React, { Component } from 'react'

export default class BaiTapState extends Component {

    constructor(props) {
        super(props);
        this.state = {
            img: "/newImgs/black-car.jpg"
        }
    }


    hamThayDoiMau = (event) => {
        let imgSource = '';
        switch (event.target.innerText) {
            case "Red":
                imgSource = "/newImgs/red-car.jpg"
                break;
            case "Sliver":
                imgSource = "/newImgs/silver-car.jpg"
                break;
            case "Black":
                imgSource = "/newImgs/black-car.jpg"
                break;
            default:
                break;
        }
        this.setState({
            img: imgSource
        })
    }
    render() {
        return (
            <div>
                <div className="d-flex">
                    <div className="col-6">
                        <img className='w-100' src={this.state.img} alt="" />
                    </div>
                    <div className="col-6 py-5 my-5 border-0">
                        <button onClick={(event) => {
                            this.hamThayDoiMau(event)
                        }}
                            className='col-4 bg-danger  border-0 rounded btn'>Red</button>
                        <button onClick={(event) => {
                            this.hamThayDoiMau(event)
                        }} className='col-4  border-0 rounded btn bg-success'>Sliver</button>
                        <button onClick={(event) => {
                            this.hamThayDoiMau(event)
                        }} className='col-4 bg-dark   border-0 rounded btn text-light'>Black</button>
                    </div>
                </div>
            </div>
        )
    }
}

