import React, { Component } from 'react'

export class DemoVongLap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mangSanPham: [
                {
                    maSP: 1, tenSP: "Iphone X", gia: 1000
                },
                {
                    maSP: 2, tenSP: "Huawei mate 20 pro", gia: 2000
                },
                {
                    maSP: 3, tenSP: "Samsung galaxy note 10", gia: 3000
                },
            ]
        }
    }
    // ! thuộc tính
    renderSanPham = () => {
        let mangMoi = this.state.mangSanPham.map((sanPham) => {
            return (
                <tr key={sanPham.maSP} >
                    <td>{sanPham.maSP}</td>
                    <td>{sanPham.tenSP}</td>
                    <td>{sanPham.gia}</td>
                </tr>
            )
        })
        return mangMoi;
    }
    render() {
        return (
            <>
                <div className="container">
                    <h3 className='bg-dark p5 text-center text-white' >Demo Vòng lặp React</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Mã sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá sản phẩm</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderSanPham()}
                        </tbody>
                    </table>
                </div>
            </>
        )
    }
}

export default DemoVongLap