import React, { Component } from 'react'
import Footer from './Footer'
import Header from './Header'
import Modal from './Modal'
import ProductList from './ProductList'
import Slider from './Slider'

export default class BaiTapComponent extends Component {

    constructor(props) {
        super(props);
        this.state = (

            {
                sanPhamChiTiet: {}
            }
        )
    }
    mangSanPham = [
        {
            maSP: 1, tenSP: "Black Berry", hinhAnh: "bestImgs/blackberry.jpg"
        },
        {
            maSP: 2, tenSP: "Iphone X", hinhAnh: "bestImgs/iphone-x.jpg"
        },
        {
            maSP: 3, tenSP: "Note 7", hinhAnh: "bestImgs/note-7.jpg"
        },
        {
            maSP: 4, tenSP: "Vivo", hinhAnh: "bestImgs/vivo.jpg"
        }
    ]
    layThongTin = (maSP) => {
        let spCanTim = this.mangSanPham.find((sp) => {
            return sp.maSP == maSP;
        })
        this.setState({
            sanPhamChiTiet: spCanTim
        }, () => {
            console.log(this.state);
        })
    }
    render() {
        return (
            <div>
                <Header />
                <Slider />
                <ProductList
                    mangSanPham={this.mangSanPham}
                    layThongTin={this.layThongTin} />
                <Footer />
                <Modal doiTuong={this.state.sanPhamChiTiet} />
            </div>
        )
    }
}
