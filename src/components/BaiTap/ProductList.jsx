import React, { Component } from 'react'
import Product from './Product'
export default class ProductList extends Component {
    constructor(props) {
        super(props);
    }


    renderSanPham = () => {
        let { mangSanPham, layThongTin } = this.props;
        let sp = mangSanPham.map((item) => {
            return (
                <div className='col-3' key={item.maSP} >
                    <Product
                        duLieu={item}
                        layThongTin={layThongTin}
                    />
                </div>)
        })
        return sp;
    }

    render() {
        return (
            <div >
                <h3>Best SmartPhone</h3>
                <div className="container">
                    <div className="row justify-content-center">
                        {this.renderSanPham()}
                    </div>
                </div>
            </div>
        )
    }
}
