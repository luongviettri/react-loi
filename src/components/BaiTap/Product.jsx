import React, { Component } from 'react'

export default class Product extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        let { duLieu, layThongTin } = this.props;
        return (
            <div >
                <div className="card ">
                    <div className="card-body">
                        <h4 className="card-title">{duLieu.tenSP}</h4>
                        <img className='w-100' src={duLieu.hinhAnh} alt="" />
                        <a onClick={() => {
                            layThongTin(duLieu.maSP)
                        }} href="" className='btn btn-warning' data-toggle="modal" data-target="#modelId" >Detail</a>
                    </div>
                </div>

            </div>
        )
    }
}
