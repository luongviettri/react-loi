import React, { Component } from 'react'

export default class Event extends Component {
    // Phương thức xử lý

    handleShowMessage = (message) => {
        alert(message);
    }

    render() {
        let message = "viet tri"

        return (
            <div className='container'>
                {/* <button onClick={this.handleShowMessage.bind(this, message)} >
                    click me
                </button> */}
                <button
                    onClick={
                        () => {
                            this.handleShowMessage(message);
                        }
                    } >
                    click me
                </button>
            </div>
        )
    }
}
