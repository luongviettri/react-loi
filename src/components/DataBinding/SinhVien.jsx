import React, { Component } from 'react'

export default class SinhVien extends Component {
    // ! thuộc tính
    hoTen = "Nguyễn Văn A";
    lop = "Front end BC04";
    tenTrungTam = "CYBERSORT";

    // ! phương thức

    renderThongTin = () => {
        return (
            <ul>
                <li>Họ tên: {this.hoTen}</li>
                <li>Lớp: {this.lop} </li>
                <li>Trung tâm: {this.tenTrungTam} </li>
            </ul>
        )
    }
    render() {
        // ! biến của hàm render
        return (
            <div>
                {this.renderThongTin()}
            </div>

        )
    }
}
