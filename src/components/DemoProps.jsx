import React, { Component } from 'react';

class DemoProps extends Component {
    constructor(props) {
        super(props);
        let { title } = this.props
    }
    render() {
        return (
            <div>
                Hello {this.props.title}
            </div>
        );
    }
}

export default DemoProps;